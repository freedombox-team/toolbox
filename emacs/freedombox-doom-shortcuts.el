;;; ~/.doom.d/freedombox-doom-shortcuts.el -*- lexical-binding: t; -*-

;; This file is part of FreedomBox.
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(map! :leader
  ;;; <leader> F --- FreedomBox
  (:prefix-map ("F" . "FreedomBox")
   (:prefix ("p"   . "Go to project")
    :desc "FreedomBox" "p"     #'freedombox-project-freedombox)
   (:prefix ("i"   . "Issues")
    :desc "List" "l"       #'freedombox-list-issues
    :desc "Create" "c"     #'freedombox-new-issue-freedombox
    :desc "Browse" "i"     #'freedombox-goto-issue)
   (:prefix ("m"   . "Merge Requests")
    :desc "List" "l"       #'freedombox-list-merge-requests
    :desc "Apply" "a"      #'freedombox-apply-merge-request)
   :desc "Activity" "a"     #'freedombox-project-activity
   :desc "Chat" "c"         #'freedombox-chat
   :desc "Kanban Board" "b" #'freedombox-kanban-board
   :desc "Forum" "f"        #'freedombox-forum
   (:prefix ("n"   . "News")
    :desc "FreedomBox Foundation News" "f" #'freedombox-foundation-news
    :desc "Mastodon" "m"   #'freedombox-mastodon-toots
    :desc "Videos" "v"     #'freedombox-videos)
   (:prefix ("r" . "Reference")
    :desc "Developer Documentation" "d" #'freedombox-developer-documentation
    :desc "Refer to Manual Page" "m" #'freedombox-manual-page)
   :desc "Translation" "t"  #'freedombox-translation
   :desc "Website" "w"      #'freedombox-website
   :desc "Voice calls" "v"  #'freedombox-voice-calls))
